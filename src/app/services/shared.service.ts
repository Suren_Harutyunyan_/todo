import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor() {
  }
   addTask(data) {
    localStorage.setItem('tasks', JSON.stringify(data));
   }
   allTasks() {
    return localStorage.getItem('tasks');
  }
}
