import { Component, OnInit } from '@angular/core';
import { Task } from '../interfaces/task';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  tasks: any = [{
    title: 'Сделать кофе',
    state: false,
  }];
  newTask: Task = {
    title: '',
    state: false,
  };

  constructor(private sharedService: SharedService) {
  }

  ngOnInit() {
    if (localStorage.getItem('tasks')) {
      const data = this.sharedService.allTasks();
      this.tasks = JSON.parse(data);
    }
  }

  addTask(): void {
    const data = this.newTask;
    this.newTask = {
      title: '',
      state: false,
    };
    this.tasks.push(data);
    this.saveTask();
  }

  saveTask(): void {
    this.sharedService.addTask(this.tasks);
  }

  removeTask(index) {
    this.tasks.splice(index, 1);
    this.saveTask();
  }

  answer2(func, delay, n) {
    let state: boolean = true;
    let calls: number = 0;
    if (state) {
      if (calls < n) {
        calls++;
        func();
      } else {
        state = false;
        calls = 0;
        setTimeout(() => {
          state = true;
        }, delay || 1000);
      }
    }
  }
}
